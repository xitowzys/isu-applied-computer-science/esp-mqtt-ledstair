import time
import paho.mqtt.client as client
import random
import codecs

color_led = {
    "null": "000000",
    "red": "110000",
    "green": "001100",
    "blue": "000011",
}

# 00 00 11 110000 110000

def generate_led(leds: list):
    result = ""

    for byte in leds:
        result += byte

    return bytes.fromhex(result)


if __name__ == "__main__":
    test = generate_led([
        color_led["blue"],
        color_led["green"],
        color_led["red"],
        color_led["blue"],
        color_led["green"],
        color_led["red"],
        color_led["blue"],
        color_led["green"],
        color_led["red"],
        color_led["blue"],
        color_led["green"],
        color_led["red"],
    ])

    print(test)
    print(type(test))


    broker = "broker.emqx.io"

    client = client.Client('isu10012300')

    print("Connecting to broker", broker)
    print(client.connect(broker))
    client.loop_start()
    print("Publishing")

    # state = "255255255000000000000000000000000000000000000"
    client.publish("isu8266-00/lab/leds/strip/set_leds_bytes", test)
    time.sleep(2)


    # for i in range(10):
    #     state = "255000000000255000255000000000255000000000255"
    #     state = state[i:] + state[:i]
    #     print(f"state is {state}")
    #     client.publish("isu8266-00/lab/leds/strip/set_leds", state)
    #     time.sleep(2)

    client.disconnect()
    client.loop_stop()
