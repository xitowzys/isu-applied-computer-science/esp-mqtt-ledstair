#include "Config.h"
#include "WIFI.h"
#include "Server.h"
#include "MQTT.h"


unsigned int distance_sm=0;
unsigned int time_us=0;

const int Trig = 0;
const int Echo = 2; 

void setup() {
  Serial.begin(9600);
  WIFI_init(false);
  server_init();
  MQTT_init();
  mqtt_cli.publish("1410120405/led_stairs/front", "start_publishing");
  pinMode(Trig, OUTPUT); // Trig 1 for 10mks
  pinMode(Echo, INPUT); 
  Serial.println("test1");
}

void loop() {
  // put your main code here, to run repeatedly:

  digitalWrite(Trig, HIGH); // Подаем сигнал на выход микроконтроллера
  delayMicroseconds(10); 
  digitalWrite(Trig, LOW); 
  time_us=pulseIn(Echo, HIGH);
  distance_sm=time_us/58; 
  Serial.println(distance_sm);
  String numberString = String(distance_sm);
  //char buffer[7];
  //itoa(distance_sm, buffer,10);
  mqtt_cli.publish(".../led_stairs/...", String(distance_sm).c_str()); // fron or rear
  delay(1000);  
  //Serial.println(1);/
  server.handleClient();                   
  mqtt_cli.loop();
}
